const express = require('express')
const port = 4000;
const app = express();
app.use(express.json());

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

// HTTP GET METHOD (home)
	app.get("/home", (request, response)=>{
		response.send("Hello there!");
	});

// HTTP GET METHOD (items)
	app.get("/items", (request, response)=>{
		response.send(items);
	});

// HTTP DELETE METHOD(delete-item)
	app.delete("/delete-item/:index", (request, response)=>{
		let indexToBeDeleted = request.params.index;
		indexToBeDeleted = parseInt(indexToBeDeleted);

		if(indexToBeDeleted<items.length){

			response.send(`The user ${items[indexToBeDeleted].name} is now deleted!`)
		}else {
			response.status(404).send(`Page Not Found!`)
		}
		
	})

app.listen(port, ()=> console.log(`The server is now running at port: ${port}`))